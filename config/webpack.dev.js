const webpack = require('webpack');
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const hote = 'webpack-hot-middleware/client?name=e_app&path=/__webpack_hmr&timeout=20000&reload=true';
const hotf = 'webpack-hot-middleware/client?name=f_app&path=/__webpack_hmr&timeout=20000&reload=true';

module.exports = {
  mode: 'development',
  devtool: 'eval',
  entry: {
    c_app: path.join(process.cwd(), './src/core/c_app/index.js'),
    d_app: path.join(process.cwd(), './src/core/d_app/index.js'),
    e_app: ['react-hot-loader/patch', path.join(process.cwd(), './src/core/e_app/index.js'), hote],
    f_app: ['react-hot-loader/patch', path.join(process.cwd(), './src/core/f_app/index.js'), hotf],
    g_app: ['react-hot-loader/patch', path.join(process.cwd(), './src/core/g_app/index.js'), hotf],
  },
  output: {
    filename: 'assets/[name].js',
    publicPath: '/',
    path: path.join(process.cwd(), '../dist/'),
  },
  module: {
    rules: [
      {
        test: /\.js|jsx$/,
        enforce: 'pre',
        exclude: /node_modules/,
        use: {
          loader: 'eslint-loader',
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          cacheDirectory: true,
          plugins: ['react-hot-loader/babel'],
        },
      },
      {
        test: /\.css|.styl$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          'stylus-loader',
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx', '.styl', '.json'],
    modules: ['node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  watchOptions: {
    ignored: ['config/**', 'dist/**', 'node_modules/**'],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      filename: 'assets/[name].css',
    }),
  ],
};
