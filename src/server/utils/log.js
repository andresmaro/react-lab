import clc from 'cli-color';

const LOG_LEVEL = process.env.LOG_LEVEL || 'info';

export const info = (message) => {
  if (['info'].includes(LOG_LEVEL)) {
    console.log(clc.green(`[maro:pg] ${message}`));
  }
};

export const warn = (message) => {
  if (['info', 'warn'].includes(LOG_LEVEL)) {
    console.warn(clc.yellow(`[maro:pg] ${message}`));
  }
};

export const error = (message) => {
  if (['info', 'warn', 'error'].includes(LOG_LEVEL)) {
    console.error(clc.red(`[maro:pg] ${message}`));
  }
};

export default { info, warn, error };
