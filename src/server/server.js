import express from 'express';
import router from './routes/router';
import webpackMiddleware from './middlewares/webpack';
import { info } from './utils/log';

info('::: starting :::');

const app = express();
const port = process.env.PORT;

app.use(express.static('src/static'));

webpackMiddleware(app);

app.use('/', router);

app.listen(port, () => {
  info(`::: listening on http://localhost:${port} :::`);
});
