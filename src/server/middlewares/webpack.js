import webpack from 'webpack';
import path from 'path';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../../../config/webpack.dev';
import { warn } from '../utils/log';

const compiler = webpack(config);

const webpackMiddleware = (app) => {
  warn('::: using webpack dev middleware :::');
  app.use(webpackDevMiddleware(compiler,
    {
      publicPath: config.output.publicPath,
      contentBase: path.resolve(__dirname, '../src'),
      writeToDisk: false,
    }));
  app.use(webpackHotMiddleware(compiler));
};

export default webpackMiddleware;
