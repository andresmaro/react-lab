import express from 'express';
import path from 'path';
import cApp from './router/c_app';
import dApp from './router/d_app';
import eApp from './router/e_app';
import fApp from './router/f_app';
import gApp from './router/g_app';

const router = express.Router();

router.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/router/index.html'));
});

router.get('/a', (req, res) => res.sendFile(path.join(__dirname + '/router/a.html')));
router.get('/b', (req, res) => res.sendFile(path.join(__dirname + '/router/b.html')));
router.get('/c', (req, res) => cApp(req, res));
router.get('/d', (req, res) => dApp(req, res));
router.get('/e', (req, res) => eApp(req, res));
router.get('/f', (req, res) => fApp(req, res));
router.get('/g', (req, res) => gApp(req, res));

export default router;
