import React from 'react';
import { renderToString } from 'react-dom/server';
import { renderHead, renderBottom } from './f';
import App from '../../../core/f_app/app';

const context = 'f_app';

const app = (req, res) => {
  res.write(renderHead(context));
  res.write(renderToString(<App />));
  res.write(renderBottom(context));
  res.end();
};

export default app;
