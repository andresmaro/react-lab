export const renderBody = (context) => (`
<body>
  <section id=${context}></section>
  <script src="/assets/c_app.js"></script>
</body>
`)
