export const renderHead = () => (`
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Raw JS webpack</title>
  <link rel="icon" href="/favicon.ico">
  <link rel="stylesheet" href="/style/global.css">
</head>
`)