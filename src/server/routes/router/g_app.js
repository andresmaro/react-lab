import React from 'react';
import { renderToString } from 'react-dom/server';
import { renderHead, renderBottom } from './g';
import App from '../../../core/g_app/app';

const context = 'g_app';

const app = (req, res) => {
  res.write(renderHead(context));
  res.write(renderToString(<App />));
  res.write(renderBottom(context));
  res.end();
};

export default app;
