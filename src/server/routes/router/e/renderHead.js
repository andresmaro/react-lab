export const renderHead = (context) => (`
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/assets/${context}.css" type="text/css" />
  <title>HMR and styles</title>
  <link rel="icon" href="/favicon.ico">
  <link rel="stylesheet" href="/style/global.css">
</head>
<body>
  <section id=${context}>`);
