import React from 'react';
import { renderToString } from 'react-dom/server';
import { renderHead, renderBottom } from './d';
import App from '../../../core/d_app/app';

const context = 'd_app';

const app = (req, res) => {
  res.write(renderHead(context));
  res.write(renderToString(<App />));
  res.write(renderBottom(context));
  res.end();
};

export default app;
