import { renderHead, renderBody } from './c';

const context = 'c_app';
const app = (req, res) => {
  res.write('<!DOCTYPE html><html lang="en">');
  res.write(renderHead());
  res.write(renderBody(context));
  res.write('</html>');
  res.end();
};

export default app;
