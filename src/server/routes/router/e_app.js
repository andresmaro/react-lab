import React from 'react';
import { renderToString } from 'react-dom/server';
import { renderHead, renderBottom } from './e';
import App from '../../../core/e_app/app';

const context = 'e_app';

const app = (req, res) => {
  res.write(renderHead(context));
  res.write(renderToString(<App />));
  res.write(renderBottom(context));
  res.end();
};

export default app;
