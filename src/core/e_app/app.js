import { hot } from 'react-hot-loader/root';
import React from 'react';
import './styles/app.styl';

const App = props => {
  return (
    <div className="app">
      SSR | Hot Module Replacement | Styles
    </div>
  );
};

export default hot(App);
