import React from 'react';
import './styles/TextInput.styl';

export const TextInput = props => {
  return (
    <input type="text" {...props} className={`TextInput ${props.className}`} />
  );
};

