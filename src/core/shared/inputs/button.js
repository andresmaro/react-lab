import React from 'react';
import './styles/Button.styl';

export const Button = (props) => {
  return (
    <button {...props} className={`Button ${props.className}`} />
  );
};
