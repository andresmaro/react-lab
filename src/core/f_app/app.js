import { hot } from 'react-hot-loader/root';
import React from 'react';
import { CommonHooks, HookFlow, LiftingState, TicTacToe } from './components';
import './styles/app.styl';

const App = props => {
  return (
    <div className="app">
      <CommonHooks />
      <HookFlow />
      <LiftingState />
      <TicTacToe />
    </div>
  );
};

export default hot(App);
