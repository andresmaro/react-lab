import React, { useState } from 'react';
import { TextInput } from '../../shared/inputs';

function Name({ name, onNameChange }) {
  return (
    <div>
      <label htmlFor="name">Name: </label>
      <TextInput id="name" value={name} onChange={onNameChange} />
    </div>
  );
}

function FavoriteAnimal({ animal, onAnimalChange }) {

  return (
    <div>
      <label htmlFor="animal">Favorite Animal: </label>
      <TextInput
        id="animal"
        value={animal}
        onChange={onAnimalChange}
      />
    </div>
  );
}

function Display({ name, animal }) {
  return <div>{`Hey ${name}, your favorite animal is: ${animal}!`}</div>;
}

export const LiftingState = () => {
  const [name, setName] = useState('');
  const [animal, setAnimal] = useState('');

  const onAnimalChange = (event) => {
    setAnimal(event.target.value);
  };

  return (
    <form>
      <Name name={name} onNameChange={event => setName(event.target.value)} />
      <FavoriteAnimal animal={animal} onAnimalChange={onAnimalChange}/>
      <Display name={name} animal={animal}/>
    </form>
  );
};
