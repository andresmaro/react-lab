import React from 'react';
import { useLocalstorage } from '../hooks';
import { TextInput } from '../../shared/inputs';

export const CommonHooks = props => {

  const [username, setUsername] = useLocalstorage('name', 'Guest');

  const onChange = (event) => {
    setUsername(event.target.value);
  };

  return (
    <div>
      <TextInput value={username} onChange={onChange} />
      <span style={{ marginLeft: '1rem' }}>{`Hello ${username}`}</span>
    </div>
  );
};

