import React from 'react';
import { hydrate } from 'react-dom';
import App from './app';

const hello = document.getElementById('f_app');
hydrate(<App />, hello);
