const { useState, useEffect } = require('react');

export const useLocalstorage = (key, defaultVal) => {
  if (typeof window === 'undefined') return ['', null];
  const [state, setState] = useState(
    () => window.localStorage.getItem(key) || defaultVal
  );
  useEffect(() => {
    window.localStorage.setItem(key, state);
  }, [key, state]);

  return [state, setState];
};
