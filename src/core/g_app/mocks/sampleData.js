import { name, phone, lorem } from 'faker';
import dayjs from 'dayjs';

const today = dayjs();
const at = ([h, m]) => today.hour(h).minute(m).format('hh:mm a');

const services = [
  'Cut',
  'Blow-dry',
  'Cut & color',
  'Beard trim',
  'Cut & beard trim',
  'Extensions',
];

const getRandomStylist = () => {
  [0, 1, 2, 3, 4, 5, 6]
    .map(() => name.firstName())
    .filter(function (value, index, self) {
      return self.indexOf(value) === index;
    });
};

const getRandomService = () => services[Math.floor(Math.random() * services.length)];

const generateFakeCustomer = () => ({
  firstName: name.firstName(),
  lastName: name.lastName(),
  phoneNumber: phone.phoneNumberFormat(2),
});

const generateFakeAppointment = () => ({
  customer: generateFakeCustomer(),
  stylist: getRandomStylist(),
  service: getRandomService(),
  notes: lorem.paragraph(),
});

export const sampleAppointments = [
  { startsAt: at([9, 0]), ...generateFakeAppointment() },
  { startsAt: at([9, 30]), ...generateFakeAppointment() },
  { startsAt: at([11, 0]), ...generateFakeAppointment() },
  { startsAt: at([11, 50]), ...generateFakeAppointment() },
  { startsAt: at([12, 45]), ...generateFakeAppointment() },
  { startsAt: at([14, 0]), ...generateFakeAppointment() },
  { startsAt: at([15, 45]), ...generateFakeAppointment() },
  { startsAt: at([16, 30]), ...generateFakeAppointment() },
  { startsAt: at([18, 0]), ...generateFakeAppointment() },
];
