import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Appointment } from './';

export const AppointmentsDay = ({ defaultText, appointments }) => {

  const [currentIndex, setcurrentIndex] = useState(0);

  const doClick = (index) => () => setcurrentIndex(index);

  return (
    <div className="AppointmentsDay">
      {appointments?.length > 0 ?
        <ol>
          {appointments.map((appoinment, index) => (
            <li key={`appoinment-${index}`}>
              <button onClick={doClick(index)}>{appoinment.startsAt}</button>
            </li>
          ))}
        </ol> :
        <p>{defaultText}</p>
      }
      <Appointment customer={appointments[currentIndex]?.customer} />
    </div>
  );
};

AppointmentsDay.propTypes = {
  defaultText: PropTypes.string,
  appointments: PropTypes.array,
};

AppointmentsDay.defaultProps = {
  appointments: [],
};
