import React from 'react';
import PropTypes from 'prop-types';

export const Appointment = ({ customer }) => {
  return (
    <div className="Appointment">
      {customer?.firstName}
    </div>
  );
};

Appointment.propTypes = {
  customer: PropTypes.shape({
    firstName: PropTypes.string,
  }),
};
