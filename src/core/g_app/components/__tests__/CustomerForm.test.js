import 'regenerator-runtime/runtime';

import React from 'react';
import { render, screen } from '@testing-library/react';
import { Simulate } from 'react-dom/test-utils';
import { CustomerForm } from '../';

describe('CustomerForm', () => {
  let container, form;

  beforeEach(() => {
    ({ container } = render(<CustomerForm firstName={'Ashley'} />));
    form = container.querySelector('form#customer');
  });

  it('renders a form', () => {
    expect(form).not.toBeNull();
  });

  it('renders the first name field as a text box', () => {
    const field = form.elements.firstName;
    expect(screen.getAllByLabelText('First Name')).toBeTruthy();
    expect(field).toBeTruthy();
    expect(field.tagName).toEqual('INPUT');
    expect(field.type).toEqual('text');
    expect(field.value).toEqual('Ashley');
  });

  it.skip('saves existing first name when submitted', async () => {
    expect.hasAssertions();
    render(
      <CustomerForm
        firstName={'Ashley'}
        onSubmit={({ firstName }) => expect(firstName).toEqual('Ashley')}
      />);

    await Simulate.change(firstNameField(), {
      target: { value: 'Jamie' },
    });

    await Simulate.submit(form('customer'));
  });
});
