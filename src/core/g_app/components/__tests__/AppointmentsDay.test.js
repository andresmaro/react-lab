import React from 'react';
import ReactDOM from 'react-dom';
import dayjs from 'dayjs';
import { Simulate } from 'react-dom/test-utils';
import { AppointmentsDay } from '../';

describe('AppointmentsDay', () => {
  let container;
  const date = dayjs();

  const appointments = [
    {
      startsAt: date.hour(11).minute(30).format('hh:mm a'),
      customer: { firstName: 'Ashley' },
    },
    {
      startsAt: date.hour(13).minute(45).format('hh:mm a'),
      customer: { firstName: 'Jordan' },
    },
  ];

  const render = (component) => ReactDOM.render(component, container);

  beforeEach(() => {
    container = document.createElement('div');
  });

  it('renders the correct class nane and default message', () => {
    const defaultText = 'No appointments for today';
    render(<AppointmentsDay defaultText={defaultText} />);
    expect(container.querySelector('.AppointmentsDay').textContent).toMatch(defaultText);
  });

  it('renders and ol element in the root', () => {
    render(<AppointmentsDay appointments={appointments} />);
    expect(container.querySelector('ol').children).toHaveLength(2);
  });

  it('shows the correct formated hour', () => {
    render(<AppointmentsDay appointments={appointments} />);
    expect(container.querySelectorAll('li')[0].textContent).toMatch('11:30 am');
  });

  it('has a button in every li element', () => {
    render(<AppointmentsDay appointments={appointments} />);
    expect(container.querySelectorAll('li > button')).toHaveLength(2);
  });

  it('renders name of the first selected item', ()=>{
    render(<AppointmentsDay appointments={appointments} />);
    expect(container.querySelector('.Appointment').textContent).toMatch('Ashley');
  });

  it('renders name of the selected item', ()=>{
    render(<AppointmentsDay appointments={appointments} />);
    const button = container.querySelectorAll('button')[1];
    Simulate.click(button);
    expect(container.querySelector('.Appointment').textContent).toMatch('Jordan');
  });

});
