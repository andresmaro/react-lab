import React from 'react';

export const CustomerForm = ({ firstName, onSubmit }) => {
  return (
    <div className="CustomerForm">
      <form id="customer" onSubmit={onSubmit}>
        <label htmlFor="firstName">First Name</label>
        <input id="firstName" type="text" value={firstName} readOnly />
      </form>
    </div>
  );
};

