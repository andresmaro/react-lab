import { hot } from 'react-hot-loader/root';
import React from 'react';
import { sampleAppointments } from './mocks/sampleData';
import { AppointmentsDay } from './components';

const App = () => {
  return (
    <div className="app">
      <AppointmentsDay appointments={sampleAppointments} />
    </div>
  );
};

export default hot(App);
